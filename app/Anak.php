<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    //
    protected $table = 'anaks';
    protected $primaryKey = 'id_anak';

    public function ortu()
    {
        return $this->belongsTo('App\User', 'id_ibu');
    }
}
