<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuTamu extends Model
{
    protected $table = 'buku_tamus';
    protected $primaryKey = 'id_tamu';

    public function kegiatan()
    {
        return $this->hasMany('App\Kegiatan', 'id_tamu');
    }
}
