<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class IbuImunisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('imunisasis as M')
                ->leftjoin('anaks as A', 'M.id_anak', '=', 'A.id_anak')
                ->leftjoin('users as I', 'A.id_ibu', '=', 'I.id')
                ->leftjoin('jenis_imunisasis as J', 'M.id_j_imun', '=', 'J.id_j_imun')
                ->where('A.id_ibu' , '=' , auth()->user()->id)
                ->select('M.id_imun', 'A.nama_anak', 'J.nama_imun', 'M.tgl_imun', 'M.booster', 'M.ket_imun')
                ->orderBy('id_imun', 'asc')
                ->get();
        return view('user.imunisasi.view', compact('data'));

        // return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
