<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisImunisasi extends Model
{
    protected $table = 'jenis_imunisasis';
    protected $primaryKey = 'id_j_imun';

    public function imunisasi()
    {
        return $this->hasMany('App\Imunisasi', 'id_j_imun');
    }
}
