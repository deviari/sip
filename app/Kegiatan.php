<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table = 'kegiatans';
    protected $primaryKey = 'id_kegiatan';

    public function bukuTamu()
    {
        return $this->belongsTo('App\BukuTamu', 'id_tamu');
    }

    public function agenda()
    {
        return $this->belongsTo('App\Agenda', 'id_agenda');
    }

    public function kehadiran()
    {
        return $this->hasMany('App\Kehadiran', 'id_kegiatan');
    }
}
