<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kehadiran extends Model
{
    //
    protected $table = 'kehadirans';
    protected $primaryKey = 'id_kehadiran';

    public function anak()
    {
        return $this->belongsTo('App\Anak', 'id_anak');
    }
}
