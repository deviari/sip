<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukm extends Model
{
    protected $table = 'ukms';
    protected $primaryKey = 'id_ukm';

    public function kegiatan()
    {
        return $this->hasMany('App\Kegiatan', 'id_ukm');
    }
}
