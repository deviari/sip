<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Anak</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body>
    <table width="100%" style="text-align: center; margin: 0px; font-size: 11px;">
        <tr>
            <td>
                <p style="font-size: 11px; margin: 0px">
                    <strong>DAFTAR HADIR</strong>
                </p>
            </td>
        </tr>
    </table>
    <table width="100%" style="font-size: 11px">
        <tr>
            <th>KEGIATAN</th>
            <td>: {{ $agenda->kegiatan }}</td>
            <th></th>
            <td></td>
        </tr>
        <tr>
            <th>KELURAHAN</th>
            <td>: SUMBERREJO</td>
            <th>PUSKESMAS</th>
            <td>: BENOWO</td>
        </tr>
        <tr>
            <th>RW/RT</th>
            <td>: 1/01-04</td>
            <th>BULAN</th>
            <td>: {{ \Carbon\Carbon::parse($agenda->start)->format('F') }}</td>
        </tr>
        <tr>
            <th>NAMA POSYANDU</th>
            <td>: MANDIRI SUMBERREJO</td>
            <th>TANGGAL</th>
            <td>: {{ \Carbon\Carbon::parse($agenda->start)->format('d-m-Y') }}</td>
        </tr>
    </table>
    <br>
    <table width="100%" style="font-size: 11px; border: 1px solid black; border-collapse: collapse;">
        <thead style="text-align: center;">
            <tr>
                <th rowspan="2" style="border: 1px solid black">NO</th>
                <th rowspan="2" style="border: 1px solid black">NAMA BALITA</th>
                <th rowspan="2" style="border: 1px solid black">L/P</th>
                <th colspan="2" style="border: 1px solid black">NAMA ORANG TUA</th>
                <th rowspan="2" style="border: 1px solid black">ALAMAT/RT</th>
                <th rowspan="2" colspan="2" style="border: 1px solid black">TANDA TANGAN</th>
            </tr>
            <tr>
                <th style="border: 1px solid black">BAPAK</th>
                <th style="border: 1px solid black">IBU</th>
            </tr>
        </thead>
        <tbody>
            @php($nomor = 1)
            @foreach($kegiatan as $dataKegiatan)
                @foreach($dataKegiatan->kehadiran as $kehadiran)
                    <tr>
                        <td style="border: 1px solid black; text-align: center;">
                            {{ $nomor }}
                        </td>
                        <td style="border: 1px solid black;">
                            {{ $kehadiran->anak->nama_anak }}
                        </td>
                        <td style="border: 1px solid black; text-align: center;">
                            {{-- 0: Laki-laki, 1: Perempuan --}}
                            {{ $kehadiran->anak->jenis_kelamin ? 'P' : 'L' }}
                        </td>
                        <td style="border: 1px solid black;">
                            {{ $kehadiran->anak->ortu->nama_suami }}
                        </td>
                        <td style="border: 1px solid black;">
                            {{ $kehadiran->anak->ortu->nama_ibu }}
                        </td>
                        <td style="border: 1px solid black;">
                            {{ $kehadiran->anak->ortu->alamat }}
                        </td>
                        <td style="border: 1px solid black;">
                            {{-- check bilangan ganjil / genap --}}
                            @if ($nomor % 2 != 0)
                            {{ $nomor }}.
                                @endif
                        </td>
                        <td style="border: 1px solid black;">
                            @if ($nomor % 2 == 0)
                                {{ $nomor }}.
                            @endif
                        </td>
                        @php($nomor++)
                    </tr>
                @endforeach
            @endforeach            
        </tbody>
    </table>
</body>
</html>
