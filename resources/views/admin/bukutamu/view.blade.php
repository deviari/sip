@extends('layouts.base')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{!! asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
@endsection

@section('breadcrumb')
<h1>
    Buku Tamu
    <small>data tamu yang berkunjung</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Buku Tamu</li>
</ol>
@endsection

@section('content')

@include('notification')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title" style="margin-right: 40px"><a href="{{ route('bukutamu.create') }}" class="btn btn-sm btn-primary"><span><i class="fa fa-plus-square" aria-hidden="true"></i></span> Tambah Data</a> | Buku Tamu</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">#</th>
                            <th class="text-center">Nama Tamu</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">Jabatan</th>
                            <th class="text-center">Keperluan</th>
                            <th class="text-center">Jml Kegiatan</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @php $no = 1; @endphp
                        @foreach($data as $datas)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $datas->nama_tamu }}</td>
                            <td>{{ $datas->alamat }}</td>
                            <td>{{ $datas->jabatan }}</td>
                            <td>{{ $datas->keperluan }}</td>
                            <td>{{ $datas->kegiatan->count() }}</td>
                            <td class="text-center">
                                <a href="{{ route('bukutamu.edit', $datas->id_tamu) }}" class=" btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
                                <button value="{{ $datas->id_tamu }}" id="btnhapus" class="btn btn-sm btn-danger" type="submit"  data-jml-kegiatan="{{ $datas->kegiatan->count() }}" data-toggle="tooltip" title="Hapus"><span class="glyphicon glyphicon-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nama Tamu</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">Jabatan</th>
                            <th class="text-center">Keperluan</th>
                            <th class="text-center">Jml Kegiatan</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection

@section('java')
<!-- DataTables -->
<script src="{!! asset('bower_components/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>

<script type="text/javascript">
    $(function () {
        $('#example1').DataTable()

        $('#example1').on('click', '#btnhapus' , function () {
            let id_tamu = $(this).val() ;
            var jml_kegiatan = $(this).data("jml-kegiatan");
            var myhtml = document.createElement("div");
            myhtml.innerHTML = "<b>"+jml_kegiatan+" KEGIATAN</b> pada buku tamu ini juga akan dihapus!!";

            $.ajax({
                type: "GET",
                url: "checkrelasi/" + id_tamu.trim() ,
                dataType: "JSON",
                success: function (response) {
                  if ( response > 0) {
                    swal({
                        title: "Yakin ingin hapus data?",
                        content: myhtml,
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                type: "GET",
                                url: "deleteBukuTamu/" + id_tamu.trim(),
                                dataType: "JSON",
                                success: function (response) {
                                   console.log(response); 
                                //    window.location.reload();
                                }
                            });
                            swal("Data berhasil dihapus", {
                            icon: "success",
                            });
                        }
                    });

                  } else {
                    console.log('langsung hapus sj')   
                  }
                }
            });
        });
    })
</script>
@endsection