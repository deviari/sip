@extends('layouts.base')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{!! asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
@endsection

@section('breadcrumb')
<h1>
    Dashboard
</h1>
<ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-medkit"></i>
                  SIPosyandu
                </h3>
            </div>
			<div class="alert alert-info alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>			
				<h1>Selamat Datang di Sistem Informasi Posyandu Mandiri</h1>
			</div>
		</div>
	</div>
</div>

@endsection
